export default ({ app, redirect }) => {
    if(app.$cookies.get("token") != null){
        redirect("/admin/accountinfo")
    }
}
