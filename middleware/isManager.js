export default ({ app, redirect }) => {
  if (
    !(
      app.$cookies.get("authority") === "ROLE_MANAGER"
    )
  ) {
    redirect("/admin/productlist/1")
    alert("You don't have the authority to add product!")
  }
};
