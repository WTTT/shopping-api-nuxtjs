module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: "Shopping Web",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Nuxt.js project" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      { rel: "icon", type: "image/x-icon", href: "/favicon.png?v=3" },
      {
        rel: "stylesheet",
        type: "text/css",
        href:
          "https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
      },
      {
        rel: "shortcut icon",
        href: "/deluxe-interior/images/favicon.ico",
        type: "/deluxe-interior/image/x-icon"
      },
      {
        rel: "apple-touch-icon",
        href: "/deluxe-interior/images/apple-touch-icon.png"
      },
      { rel: "stylesheet", href: "/deluxe-interior/css/bootstrap.min.css" },
      { rel: "stylesheet", href: "/deluxe-interior/style.css" },
      { rel: "stylesheet", href: "/deluxe-interior/css/responsive.css" },
      { rel: "stylesheet", href: "/deluxe-interior/css/custom.css" }
    ],
    script: [
      {
        src:
          "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
      },
      { src: "https://code.jquery.com/jquery-3.3.1.slim.min.js" },
      {
        src:
          "https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
      },
      { src: "/deluxe-interior/js/modernizr.js" },
      { src: "/deluxe-interior/js/all.js" },
      { src: "/deluxe-interior/js/jquery.easing.1.3.js" },
      { src: "/deluxe-interior/js/parallaxie.js" },
      { src: "/deluxe-interior/js/headline.js" },
      { src: "/deluxe-interior/js/owl.carousel.js" },
      { src: "/deluxe-interior/js/jquery.nicescroll.min.js" },
      { src: "/deluxe-interior/js/jqBootstrapValidation.js" },
      { src: "/deluxe-interior/js/contact_me.js" },
      { src: "/deluxe-interior/js/custom.js" }
    ]
  },
  plugins: ["~/plugins/main.js"],
  modules: [
    // Simple usage
    "cookie-universal-nuxt",

    // With options
    ["cookie-universal-nuxt", { alias: "cookiz" }],
    ["cookie-universal-nuxt", { parseJSON: false }],
  ],
  /*
   ** Customize the progress bar color
   */
  loading: { color: "#3B8070" },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** Run ESLint on save
     */
    vendor: ["jquery", "bootstrap"],
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        });
      }
    },
    transpile: [
      "vee-validate/dist/rules"
    ],
  }
};
